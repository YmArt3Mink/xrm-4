﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Example_04.Homework.Models.Interfaces;

namespace Example_04.Homework.FirstOrmLibrary
{
    public class FirstOrmAdapter<TDbEntity>
        : IOrmTarget<TDbEntity>
            where TDbEntity : IDbEntity
    {
        private IFirstOrm<TDbEntity> _firstOrm;

        public FirstOrmAdapter(IFirstOrm<TDbEntity> firstOrm)
        {
            _firstOrm = firstOrm;
        }

        public TDbEntity Find(int id)
        {
            return _firstOrm.Read(id);
        }

        public void ModifyOrCreate(TDbEntity entity)
        {
            bool hasEntity =
                (Find(entity.Id) != null)
                ;
            if (hasEntity)
            {
                _firstOrm.Update(entity);
                return;
            }

            _firstOrm.Create(entity); 
        }

        public void Remove(TDbEntity entity)
        {
            _firstOrm.Delete(entity);
        }
    }
}
