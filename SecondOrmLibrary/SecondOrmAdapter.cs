﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using Example_04.Homework.Models.Interfaces;

namespace Example_04.Homework.SecondOrmLibrary
{
    public class SecondOrmAdapter<TDbEntity>
        : IOrmTarget<TDbEntity>
            where TDbEntity : IDbEntity
    {
        private ISecondOrm              _secondOrm;
        private ICollection<TDbEntity>  _operableCollectionValue;
        private ICollection<TDbEntity>  _operableCollection
        {
            get
            {
                _operableCollectionValue =
                    _operableCollectionValue
                        ?? GetOperableCollection(_secondOrm.Context)
                        ;
                
                return _operableCollectionValue;
            }
            set
            {
                _operableCollectionValue = value;
            }
        }
        
        public SecondOrmAdapter(ISecondOrm secondOrm)
        {
            _secondOrm = secondOrm;
        }

        public TDbEntity Find(int id)
        {
            return
                _operableCollection
                    .FirstOrDefault(entity => entity.Id == id)
                    ;
        }

        public void ModifyOrCreate(TDbEntity entity)
        {
            var foundEntity = Find(entity.Id);
            bool hasEntity =
                (foundEntity != null)
                ;
            if (hasEntity)
            {
                _operableCollection.Remove(foundEntity);
            }

            _operableCollection
                .Add(entity)
                ;
        }

        public void Remove(TDbEntity entity)
        {
            _operableCollection.Remove(entity);
        }

        private ICollection<TDbEntity> GetOperableCollection(ISecondOrmContext context)
        {
            var entityType = typeof(TDbEntity);
            var contextFields =
                context
                    .GetType()
                    .GetFields()
                    ;
            var operableField =
                contextFields
                    .First(field => 
                        {
                            var fieldGenericType =
                                field
                                    .GetType()
                                    .GetGenericArguments()
                                    .First()
                                    ;
                            return
                                (fieldGenericType == entityType)
                                ;
                        })
                        ;
            var operableCollection = 
                (ICollection<TDbEntity>)
                    operableField.GetValue(null)
                    ;

            return operableCollection;
        }
    }
}
