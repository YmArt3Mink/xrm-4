﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Example_04.Homework.Models.Interfaces;

namespace Example_04.Homework
{
    public interface IOrmTarget<TDbEntity> 
        where TDbEntity : IDbEntity
    {
        TDbEntity Find(int id);
        void ModifyOrCreate(TDbEntity entity);

        void Remove(TDbEntity entity);
    }
}
